var button = document.querySelector('#start-button');
var output = document.querySelector('#output');

button.addEventListener('click', function() {

  let promise = new Promise((resolve, reject) => {
    setTimeout(function() { 
      resolve('https://swapi.dev/api/people/1/');
    }, 3000);
  })
    .then(url => {
      return fetch(url)
        .then(response => response.json())
        .then(data => {
          output.innerHTML = `GET: ${data.name}`
        });
    })

    .then(data => {
      return fetch('https://httpbin.org/put', {
        method: 'PUT',
        body: JSON.stringify({
          person: { name: 'Max', age: 28 }
        }),
      })
    })

    .then(data => {
      return data.json()
    })

    .then(data => {
      output.innerHTML += `<br>POST: ${data.json.person.name} ${data.json.person.age}`
    })

    .catch(error => console.log('Error'))
  

});



